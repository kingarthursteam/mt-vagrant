Vagrant-Minetest-Development-Wrapper
==========
Set up a minetest development environment in a virtual machine using Vagrant!
-----------------------------------------------------------------------------

At the moment this vagrantfile is very simple.
It simply provides a minetest server with standard configuration.

System Info
-----------

**OS:** Debian Jessie, 64 Bit  
**Minetest:** 0.4.13  
**IP-Address:** 192.168.44.23  
**Port:** 30000  



What do you need
================

* Oracle Virtual Box

* Vagrant (vagrantup.com)

* good connection


The bootstrap.sh will:

* update, upgrade

* install git, vim and minetest-server 0.4.13

* download king_arthur_game from git and king-arthur-world

* configure minetest


### How to use ?

1. install vagrant & Virtual Box
2. clone this repo to your hard drive
3. edit bootstrap.sh (GIT_USER & GIT_PWD)
4. cd to the directory, where you copied the repo to and type: `vagrant up`

Vagrant is now setting up your virtual machine by the settings in the vagrantfile.
You can stop/halt the machine by typing: `vagrant halt` / `vagrant suspend`.
You can also delete it with: `vagrant destroy`. After deleting it, you can set it up again with `vagrant up`.
If you wanna connect to the VM you have to type `vagrant ssh`.
After provisioning (happens after first `vagrant up`) you can place your mods in the mods-Directory. They are now available at server.

TODO
----

* ~~Config minetest with vagrant provisioner~~
* Support for other DBs (REDIS!)
* ~~Sync mod-dir to directory on host machine~~
* Provide several MT Versions (set it in provisioner)
* reorganize Vagrantfile, so settings can be done in a seperate file