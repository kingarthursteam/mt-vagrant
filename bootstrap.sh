#!/usr/bin/env bash

# Use single quotes instead of double quotes to make it work with special-character passwords
PROJECTFOLDER='dev_king-arthur.eu_update'
GIT_USER=''
GIT_PWD=''



# update / upgrade
sudo sh -c 'echo "deb http://ftp.de.debian.org/debian jessie-backports main" >> /etc/apt/sources.list' 
sudo apt-get update
sudo apt-get -y upgrade

# replacing shitty vi
sudo apt-get -y install vim

# install git
sudo apt-get -y install git

# install minetest
sudo apt-get -y install minetest-data=0.4.13+repack-1~bpo8+1
sudo apt-get -y install minetest-server=0.4.13+repack-1~bpo8+1

# prepare minetest installation
mkdir /home/vagrant/.minetest
cd /home/vagrant/.minetest/
mkdir worlds
mkdir games

# get king arthur game
cd games
git clone --recursive https://${GIT_USER}:${GIT_PWD}@bitbucket.org/kingarthursteam/minetest_game.git
mv minetest_game king_arthur_game
cd ..

# get king arthur world
cd worlds
wget ftp://anonymous@schoenerstedt.net/world.tar.gz
tar -xzf world.tar.gz 
rm world.tar.gz
cd ..

# minetest.conf customization
sudo sed -i 's/^#name.*/name = admin/' /etc/minetest/minetest.conf
sudo sed -i 's/^#map-dir.*/map-dir = \/home\/vagrant\/\.minetest\/worlds\/world/' /etc/minetest/minetest.conf
sudo sed -i 's/^creative_mode.*/creative_mode = true/' /etc/minetest/minetest.conf
sudo sed -i 's/^default-privs.*/default_privs = interact, shout, fly, give/' /etc/minetest/minetest.conf

# Setting rights
sudo chmod -R 777 /home/vagrant/.minetest/

# start minetest
sudo -H -u vagrant bash -c '/usr/games/minetestserver' 
